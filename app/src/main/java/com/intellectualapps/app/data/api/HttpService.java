package com.intellectualapps.app.data.api;

import com.intellectualapps.app.CustomApplication;
import com.intellectualapps.app.data.api.responses.AuthenticationResponse;
import com.intellectualapps.app.data.api.responses.DefaultResponse;
import com.intellectualapps.app.utils.ApiUtils;

import org.parceler.apache.commons.collections.MapUtils;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class HttpService {
    private static final String TAG = "HttpService";

    public HttpService() {

    }

    private static ApiService getApiService(Map<String, String> headers) {
        if (apiService == null || MapUtils.isNotEmpty(headers)) {
            apiService = ApiModule.getApiModuleInstance(CustomApplication.getCacheFile(), ApiUtils.buildHeaders(headers)).getApiService();
        }

        return apiService;
    }

    private static ApiService apiService = null;


    public static void resetApiService() {
        apiService = null;
    }

    public void authenticateUser(Map<String, String> requestMap, Callback<AuthenticationResponse> callback) {
        Call<AuthenticationResponse> call = getApiService(null).authenticateUser(requestMap);
        call.enqueue(callback);
    }
}
